use std::collections::HashMap;
use std::os::raw::c_void;
use std::ptr::null;


pub struct RuntimeParameters {
    rawData: Vec<String>,
    executeParameters: HashMap<String, String>,
}
type RunnableFunction fn(RuntimeParameters) -> c_void;
pub struct Config{
    appName: String,
    launchName: String,
    appDescription: String
}

pub struct Entry {
    prompt: String,
    name: String,
    shortDisc: String,
    longDisc: String,

    function: fn(RuntimeParameters),
}

pub struct Flag {
    title: String,

    shortID: String,
    longID: String,

    shortDisc: String,
    longDisc: String,

    hasParameter: bool,

}

pub struct Parser {
    config: Config,
    entries: Vec<Entry>,
    flags: Vec<Flag>,
}


impl Parser {

    pub fn configure(&mut self){
        self.entries.push( Entry{
            prompt: "help".parse().unwrap(),
            name: "Help".parse().unwrap(),
            shortDisc: "Get Help".parse().unwrap(),
            longDisc: format!("Get help with {}", self.config.appName),
            function: self.generateHelp
        });
    }

    pub fn parseEvaluation(options: Vec<String>) {

    }

    pub fn generateHelp(&mut self, parameters: RuntimeParameters){
        println!("{}",self.config.appName);
        println!("{}",self.config.appDescription);
        println!();
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    fn test(){
        let parser = super::Parser{
            config: Config {
                appName: String::from("Test"),
                launchName: String::from("test"),
                appDescription: String::from("Test application")
            },
            entries: vec![],
            flags: vec![]
        };
    }

    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
